// step 1 - create action constants
export const COUNTER_INCREMENT = "COUNTER/INCREMENT";
export const COUNTER_DECREMENT = "COUNTER/DECREMENT";
export const COUNTER_MULTIPLY = "COUNTER/MULTIPLY";

// step 2 - create action creators
const increment = () => {
  return {
    type: COUNTER_INCREMENT,
  };
};

const decrement = () => {
  return {
    type: COUNTER_DECREMENT,
  };
};

const multiply = (payload = 0) => {
  return {
    type: COUNTER_MULTIPLY,
    payload,
  };
};

// step 3 - export our actions
export const actions = {
  increment,
  decrement,
  multiply,
};
