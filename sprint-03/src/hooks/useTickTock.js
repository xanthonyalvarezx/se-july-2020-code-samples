import { useEffect } from "react";
import useCounter from "./useCounter";

const useTickTock = () => {
  const counter = useCounter();

  useEffect(() => {
    const timer = setInterval(() => {
      counter.increment();
    }, 1000);

    return () => {
      clearInterval(timer);
    };
    // eslint-disable-next-line
  }, []);

  return counter;
};

export default useTickTock;
