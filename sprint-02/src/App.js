import logo from "./logo.svg";
import { useState } from "react";
import { Switch, Route, useHistory, useParams } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import "./App.css";
import Counter, { getId, setCounterTo } from "./Counter";
import PeopleForm from "./PeopleForm";
import People from "./People";
import Nav from "./Nav";

const handleLogCounter = () => {
  console.log(getId());
};

const setCounterToFive = () => setCounterTo(5);

const HomePage = (props) => {
  console.log("Home Page Props", props);
  return (
    <div className="App">
      <button onClick={handleLogCounter}>Log Counter</button>
      <button onClick={setCounterToFive}>Set Counter To 5</button>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
          Learn Bruh..
        </a>
      </header>
    </div>
  );
};

const NotFound = () => <h1>404 Not Found</h1>;

const Cats = () => {
  const params = useParams();
  return params.catName ? <h3>{params.catName}</h3> : <h3>No Cat Supplied</h3>;
};

function App() {
  const history = useHistory();
  const [people, setPeople] = useState([
    { first: "Vince", age: 29, id: uuidv4() },
    { first: "Tj", age: 300, id: uuidv4() },
  ]);
  const otherProps = {
    name: "Russ",
    age: 99,
    car: "BMW",
    likesMayo: false,
  };
  const updatePeople = (person) => {
    const id = uuidv4();
    setPeople((currentPeople) => {
      const nextPerson = {
        ...person,
        id,
      };
      // add the person to the beginning of the array
      return [nextPerson, ...currentPeople];
    });
    // navigate to the person we created
    history.push(`/people/${id}`);
  };
  return (
    <>
      <Nav />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/counter">
          <Counter {...otherProps} />
        </Route>
        {/* Explain ...props */}
        <Route
          path="/people-form"
          render={(routerProps) => <PeopleForm {...routerProps} {...otherProps} updatePeople={updatePeople} />}
        />
        <Route path="/people">
          <People people={people} />
        </Route>
        <Route path="/cats/:catName?">
          <Cats />
        </Route>
        <Route path="*" component={NotFound} />
      </Switch>
    </>
  );
}

{
  /*
  <Counter name={otherProps.name} age={otherProps.age} />
 */
}

export default App;
